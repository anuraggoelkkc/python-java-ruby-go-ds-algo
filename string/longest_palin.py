def longestPalin(string):
    lp = ""
    for i in range(len(string)):
        for j in range(len(string)):
            if string[i:j] == (string[i:j])[::-1] and len(lp) < len(string[i:j]):
                lp = string[i:j]
    return lp

string = "mmmchotuutohckd"
print (longestPalin(string))