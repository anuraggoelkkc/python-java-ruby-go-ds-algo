def permutate(string,l,r):
    if l == r:
        print "".join(string)
    else:
        for i in xrange(l,r+1):
            string[l] , string[i] = string[i], string[l]
            permutate(string,l+1,r)
            string[l] , string[i] = string[i], string[l]
string = "anurag"
permutate(list(string),0,len(string)-1)
