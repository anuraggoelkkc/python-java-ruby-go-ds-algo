a = gets.to_i
array = gets.chomp.split(" ")
array = array.map { |n| n.to_i }
x = array.select { |n| n>=0 and Math.sqrt(n)%1!=0 }.max
y = array.select { |n| n<0 }.max
ans = []
if x !=nil
    ans << x
end

if y !=nil
    ans << y
end

if ans.length !=0
    puts ans.max
end